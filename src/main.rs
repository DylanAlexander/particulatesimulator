#[macro_use] extern crate text_io;

#[macro_use] extern crate coord;
use coord::prelude::*;
use coord::VecItem;
use coord::math::*;

//TODO: 
// - Implement RK4

type R3 = Vec3<f64>; //eucliean 3-space type alias
type R1 = f64; //Real numbers (ish)


const dt: f64 = 1e-5; //in s. Fundamental temporal interval.

use std::io;

fn main() {

    let a: String = read!();  //"a";//read!("{}\n");
    
    println!("Hello, world to {}!",a);


    let mut v = vec3!(1,2,3);

}




#[derive(Debug)]
struct Particle {

    m: f64, //in Kg
    q: f64, //in C

    r: R3, //locations whose components in m
    v: R3, //components in m/s
    fixed: bool, //regarding fixed
    a: R3, //in m s^-2


}

impl Particle {
    fn ke(&self) -> f64 { //KE of one particle
        //&param means function uses string but does not own it. Use &mut when mutating.
        let v = self.v.length();
        return 1./2. * self.m * v * v;
    }

    fn distance_from_origin(&self) -> R1 {
         self.r.length()
    }

    fn speed(&self) -> R1 {
        self.v.length()
    }

    fn displacement(&self, other: &Particle) -> R3 {
        //finds self - other: the vector from other to self.
        self.r - other.r
    }

    fn displace(&mut self, dx: R3) -> &Particle {
        //find new position
        self.r += dx;
        self
    }

    fn peturb_velocity(&mut self, dv: R3) -> &Particle {
        //find new particle by changing time
        self.v += dv;
        self
    
    }

    fn peturb(&mut self, a: R3) -> &Particle {
        //find new vector
        if !self.fixed {
            let dv = (self.a + a)*dt;
            //change in velocity is roughly proportional to acceleration times dt
            
            /*
            self.v += dv;

            //equivalent to newton's method
            self.r += self.v * dt;
            */
            
            //second order method (trapezoidal rule): more accurate
            //This is the Huen method - or RK2. 
            let old_v = self.v.clone();
            self.v += dv;
            self.r += (self.v + old_v)*dt/2.;
            self.a = a;
        }


        self
    }



    fn find_force_from(&self, p: Particle) -> R3 {
        //should obey newtons laws, especially the third one.
        //finds force on one particle
        //does not take stationary into account - this happense elsewhere.
        let f: R3 = vec3!(0.0,0.0,0.0);








        return f;

    }



}



fn d(p1: Particle, p2: Particle) -> R1 {
    //euclidean metric on particles

    (p1.r - p2.r).length()
}




/*trait Vector<K> where K: VecItem {
    //Utility Vectors I designed
    fn unit(&self) -> Option<Self>;


}

impl<V, K> Vector<K> for V where V: VecFloat {

    fn unit(&self: V) -> Option<V> {

        if self.length() == 0 {
            return None;
        } else {
            return Some(self / self.length());
        }

    }

}*/


